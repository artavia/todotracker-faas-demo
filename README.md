# Description
Modified todo tracker featuring bcryptjs, netlify-lambda and netlify-cli-- JAM stack / FAAS exercise --for use with Netlify and Mongo Atlas.

## Addressing some new unanticipated difficulties
**WARNING** I had difficulty with merely deleting a repository, hence, the name of the repo which is presently **todotracker-faas-demo** rather than it&rsquo;s original name of **todotracker-netlify-demo**. This matters more in the context of Angular than in React in my experience. 

## Disclaimers
  - This project has been re&#45;purposed for use with ~~Netlify Lambda Functions~~ Netlify CLI. This project showcases what it means to get down with FAAS (functions-as-a-service).
  - **A correction to the above is presently necessary!** I can confidently state, indeed, I have a finalized lambda version and a finalized cli version. You are viewing the **finalized cli version**.
  - Apparently, or at least [according to this video](https://www.youtube.com/watch?v=sakKOT6nkkE "Link to youtube"), netlify-lambda is, and, I quote "Mister sw-yx" the "old way of building [...] while Netlify CLI is the **new way** of building" and has currently taken up the mantle in terms of modern development with Netlify in 2020.
  - **BYOD...!!!**You need to furnish your own data source.
  - ~~**BYOP...!!!**You need to furnish your own proxy mechanism for local development ([visit for more info on proxying](https://create-react-app.netlify.com/docs/proxying-api-requests-in-development/ "Link to help on proxying")).~~ **This does not apply anymore when referring to Netlify CLI.**

## Please visit MERN Todo App
  - You can view [the demo which is found at netlify](https://todotracker-faas-demo.netlify.com "Link to JAM Todo App").
  - What you are seeing on this page are the **scraps** that correspond to a live Netlify site because there is no room for dev&#45;dependencies. 
  - In order for you to take a closer look at the human&#45;readable source code, I recommend that you visit the GitLab project instead.
  - ~~This project was published with the **Deploys from GitHub** option.~~
  - This project was published with the **Manual Deploys** option **as a result** of using **netlify-cli&rsquo;s netlify deploy** directive in order to push all changes to production.

## To be honest netlify&#45;lambda saved the day
The publication of this work would have been impossible without experimenting first with netlify&#45;lambda. It is easier to run **netlify dev** in the context of **create&#45;react&#45;app** than it is to run **netlify dev** in the context of **@ng/cli**. With the former it&rsquo;s like being on easy&#45;street but with the latter it is essentially non&#45;existent. 

Therefore, in the case of developing with Angular, especially, you will want to have netlify&#45;lambda at your disposal for development purposes before you can find your sea legs and move on to performing some slight refactoring (with both React and Angular) before finally using **netlify deploy** to send it all off. 

## All glory and praise be to the Lord Jesus Christ!
Yeah, yeah! I will be still.