
// =============================================
// process.env SETUP
// =============================================
const { 
  JWT_SECRET 
} = process.env;

// =============================================
// PRE SETUP
// =============================================
const jwt = require('jsonwebtoken');

// =============================================
// BASE SETUP
// =============================================

// =============================================
// process.env SETUP
// =============================================
const { 
  CONNECTION_URL 
} = process.env; 

// BASE SETUP
// =============================================
// import mongoose from 'mongoose'; // original
const mongoose = require('mongoose');

// =============================================
// DB CONNECTION 
// =============================================
mongoose.connect( CONNECTION_URL , {
  useNewUrlParser: true 
  , useUnifiedTopology: true
  , useFindAndModify: false
  , useCreateIndex: true
} );
const db = mongoose.connection;
// db.on( 'open' , console.log.bind( console, `MongoDB database connection established` ) );
// db.once( 'open' , () => {
//   console.log(  `MongoDB database connection established` );
// } );
db.on( 'error' , console.error.bind( console, 'MongoDB connection error, bif!: ' ) );
// =============================================
// END CONNECTION 
// =============================================


// =============================================
// MODEL SCHEMA
// =============================================

// const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');

const SALT_ROUNDS = 10;

// delete mongoose.connection.models['User'];

const schema = new mongoose.Schema( {

  // _id: mongoose.Schema.Types.ObjectId  
  // , 
  
  // username: { type: String }
  username: { type: String, required: true, unique: true }
  // , email: { type: String }
  , email: { type: String, required: true, unique: true }
  // , password: { type: String }
  , password: { type: String, required: true }
} );

schema.pre( "save" , function(next){
  
  // only has the pdub if new or modified
  if( !this.isModified('password') ){ 
    return next(); 
  }
  if (this.isNew || this.isModified('password')){
    
    let document = this;
    
    // asyncronously generate a salt
    bcrypt.genSalt( SALT_ROUNDS , function( err, salt ){
      
      if( err ){ 
        next(err); // return next(err); 
      } 

      // asyncronously hash the pdub using the new salt
      bcrypt.hash( document.password, salt, function( err, hashedPassword ){
        
        if( err ){ 
          next(err); // return next(err); 
        }

        document.password = hashedPassword;
        next();
      } );

    } );
  }
  else{
    next();
  }

} ); 

schema.methods.comparePdub = function(plntxtpdub, cb ){
  bcrypt.compare( plntxtpdub , this.password, function( err, isMatch ){
    if( err ){ return cb(err); } 
    // cb( err, isMatch ); // generate the generic status text error from here
    cb( null, isMatch );   // generate the specific error text from the route
  } );
}; 

// const User = mongoose.model( 'User' , schema );
let User = mongoose.connection.models.User || mongoose.model('User', schema);

// =============================================
// END MODEL SCHEMA
// =============================================


async function getUser( email ){
  return Promise.resolve( User.findOne( { email: email } ) );
}

async function getBcryptComparison( password, hash, bcrypt ){
  return Promise.resolve( bcrypt.compare( password , hash )  );
}

async function getJwtToken( jwt, secret , user ){
  let params = { user: user };
  let finerdetails = { expiresIn: '1h' };
  return Promise.resolve( jwt.sign( params, secret, finerdetails ) );
}

exports.handler = async (event, context) => {

  // accessible through...
  // http://localhost:9000/.netlify/functions/updateOneUser
  
  // context.callbackWaitsForEmptyEventLoop = false;

  // your server-side functionality 
  // console.log( "context", context ); // { clientContext: {} }
  
  // console.log( "event.path", event.path ); // "Path parameter", e.g. -- /userLogin
  // console.log( "event.httpMethod", event.httpMethod ); //"Incoming request's method name"
  // // console.log( "event.headers", event.headers ); // {Incoming request headers}
  // console.log( "event.queryStringParameters", event.queryStringParameters ); // {query string parameters }
  // console.log( "event.body", event.body ); // "A JSON string of the request payload."
  // console.log( "event.isBase64Encoded", event.isBase64Encoded ); // "A boolean flag to indicate if the applicable request payload is Base64-encode"

  let simonsays;
  
  if (event.httpMethod !== "POST") { // only POST
      
    const netlifyresponseobject = {
      statusCode: 405
      , body: JSON.stringify( { errormessage: "Method Not Allowed" } )
    };
    
    // return netlifyresponseobject;
    simonsays = netlifyresponseobject;

  }

  try{
    
    let userData = JSON.parse( await event.body );
    let { email , password } = await userData; 

    const userpromise = await getUser( await email );

    // #####################
    // if [userpromise === null] / USER NOT FOUND
    // #####################

    // console.log("whether not found or to the contrary >>>>> User.findOne userpromise " , userpromise );
    
    if( await userpromise === null ){

      // console.log( "userpromise" , await userpromise );
      
      console.log("if not userpromise...");
      // res.status(401); // Unauthorized

      const netlifyresponseerror = {
        statusCode: 401
        , body: JSON.stringify( { errormessage : "Invalid Email. Go register!" } )
      }; 
      
      simonsays = netlifyresponseerror; // return netlifyresponseerror;

    }

    // #####################
    // if [userpromise !== null] / USER FOUND
    // #####################
    if( await userpromise !== null ){

      // console.log("if userpromise...");
      // console.log( "WATCH THEE THE SECOND SCHTANKY userpromise" , await userpromise );
      
      let comparisonpromise = await getBcryptComparison( await password , await userpromise.password , bcrypt );

      // console.log( "LET'S GO GO GO GO... comparisonpromise" , await comparisonpromise );

      if( await comparisonpromise === false ){
        // console.log("Invalid Password. Try again!");
        // console.log("if await comparisonpromise === false...");
        // res.status(404); // Not Found

        const netlifyresponseerror = {
          statusCode: 404
          , body: JSON.stringify( { errormessage : "Invalid Password. Try again!" } )
        };
        
        simonsays = netlifyresponseerror; // return netlifyresponseerror;
      }

      if( await comparisonpromise === true ){
        
        // console.log("In the process of getting a signed token >>>>> ");

        let token = await getJwtToken( jwt, JWT_SECRET, await userpromise );
        
        // console.log( "LET'S GO GO GO GO... token" , await token );

        if( await !token ){
          
          console.log( "jwt sign err" );

          const netlifyresponseerror = {
            statusCode: 500
            , body: JSON.stringify( { errormessage: "jwt sign err" } )
          };
          
          simonsays = netlifyresponseerror; // return netlifyresponseerror;
        }

        //// res.status(200).json( { token: token } );

        const netlifyresponseobject = {
          statusCode: 200
          , body: JSON.stringify( { token: token } )
        };
        
        simonsays = netlifyresponseobject; // return netlifyresponseobject;

      }
      
    } 
    
    return simonsays; 

  }
  catch(err){
    
    console.log( 'user login err catch: ', err );
    
    const netlifyresponseerror = {
      statusCode: 500
      // , body: JSON.stringify( { errormessage: err.message } )
      , body: JSON.stringify( { errormessage : "Internal Error -- problem in posting data."} )
    };
    
    return netlifyresponseerror;
  }
};