// import { serverAddress } from '../services/serverAddress';

class TodoService{

  // sierra
  constructor(){
    
    // UNCOMMENT THIS AND SET PORT IN .ENV TO 3000 

    // this.serverAddress = serverAddress;
    // this.baseUrl = `${this.serverAddress}/todos`; 
    // this.baseUrl = `/todos`;        // E.G. - 'http://localhost:4000/todos'

    // port 3000 only - heroku live - port 3000 assuredly
    // this.baseUrl = `${window.location.protocol}//${window.location.hostname}:3000/todos`; 

    // port 3000 only - heroku live - port 80 on a lark but I remain skeptic
    // this.baseUrl = `${window.location.protocol}//${window.location.hostname}:${window.location.port}/todos`; 
    
    this.getAllTodos = this.getAllTodos.bind(this);
    this.createTodo = this.createTodo.bind(this);
    this.getOneTodo = this.getOneTodo.bind(this); 
    this.updateOneTodo = this.updateOneTodo.bind(this); 
    this.deleteOneTodo = this.deleteOneTodo.bind(this); 
  }

  // tango
  customfetch( url, options ){
    
    let myRequest = new Request( url );

    let myHeaders = new Headers();
    myHeaders.append( 'Content-type' , 'application/json; charset=UTF-8' ); 
    myHeaders.append( "Accept" , "application/json" );
    
    // borknalia ensues
    // if( this.loggedIn() ){
    //   myHeaders.append( "Authorization" , `Bearer ${this.getToken() }` );
    // }

    let optionsobject = { headers: myHeaders, ...options };
    // console.log( "optionsobject" , optionsobject );

    return fetch( myRequest, optionsobject )
    .then( this.checkStatus )
    .then( response => response.json() );
  }

  // uniform
  checkStatus(response) {
    if (response.status >= 200 && response.status < 300) {
      return Promise.resolve(response); // return response;
    } 
    else {
      let error = new Error( response.statusText );
      error.response = response;
      return Promise.reject( error ); // throw error;
    }
  }
  
  // victor
  createTodo( newtodoobj ){
    
    // 'http://localhost:4000/todos/add'
    // let createRequest = new Request( `${this.baseUrl}/add` );

    // let createRequest = new Request( `/.netlify/functions/createTodo` );
    // let createRequest = new Request( `/.netlify/functions/createtodo/createtodo` );
    let createRequest = new Request( `/createtodo` );

    let createOptions = {
      method: 'POST'
      , body: JSON.stringify( newtodoobj )
    };

    return this.customfetch( createRequest, createOptions )
    .then( (data) => { 
      // console.log( "createTodo... data" , data );
      return Promise.resolve( data );
    } );

  }

  // whiskey
  // v1
  getOneTodo( id ){
    
    // 'http://localhost:4000/todos/${id}'
    // let getOneRequest = new Request( `${this.baseUrl}/${id}` );
    
    // let getOneRequest = new Request( `/.netlify/functions/getOneTodo/${id}` );
    // let getOneRequest = new Request( `/.netlify/functions/getonetodo/getonetodo/${id}` ); 
    let getOneRequest = new Request( `/getonetodo/${id}` );

    let getOneOptions = {};

    return this.customfetch( getOneRequest, getOneOptions )
    .then( (data) => { 
      // console.log( "getOneTodo... data" , data );
      return Promise.resolve( data );
    } );
  } 

  // xray
  updateOneTodo( pm ){
    
    // let { id, todoObject } = pm;
    
    // 'http://localhost:4000/todos/update/${id}'
    // let editOneRequest = new Request( `${this.baseUrl}/update/${id}` );

    // let editOneRequest = new Request( `/.netlify/functions/updateOneTodo` );
    // let editOneRequest = new Request( `/.netlify/functions/updateonetodo/updateonetodo` );
    let editOneRequest = new Request( `/updateonetodo` );

    let editOneOptions = {
      method: 'POST'
      , body: JSON.stringify( pm )
    };

    return this.customfetch( editOneRequest, editOneOptions )
    .then( (data) => { 
      
      // console.log( "updateOneTodo... data" , data );

      return Promise.resolve( data );
    } );

  }
  
  // original version -- POST
  deleteOneTodo( id ){ // yankee
    
    // console.log( "id: ", id );
    
    // 'http://localhost:4000/todos/delete/${id}'
    // let deleteOneRequest = new Request( `${this.baseUrl}/delete/${id}` );

    // let deleteOneRequest = new Request( `/.netlify/functions/deleteOneTodo/${id}` );
    // let deleteOneRequest = new Request( `/.netlify/functions/deleteOneTodo` );
    // let deleteOneRequest = new Request( `/.netlify/functions/deleteonetodo/deleteonetodo` );
    let deleteOneRequest = new Request( `/deleteonetodo` );

    let deleteOneOptions = {
      method: 'POST'
      // , body: JSON.stringify( { _id: id } )
      , body: id
    };

    return this.customfetch( deleteOneRequest, deleteOneOptions )
    .then( (data) => { // console.log( "deleteOneTodo... data" , data ); // Object { lemassaj: "POST received. Hello From Netlify" }
      return Promise.resolve(data);
    } );
    
  }
  
  // new version -- GET -- borked!!!
  /* deleteOneTodo( id ){ // yankee
    
    // console.log( "id: ", id );
    
    // 'http://localhost:4000/todos/delete/${id}'
    // let deleteOneRequest = new Request( `${this.baseUrl}/delete/${id}` );

    let deleteOneRequest = new Request( `/.netlify/functions/deleteOneTodo/${id}` );
    let deleteOneOptions = { };

    return this.customfetch( deleteOneRequest, deleteOneOptions )
    .then( (data) => { // console.log( "deleteOneTodo... data" , data ); // Object { lemassaj: "POST received. Hello From Netlify" }
      return Promise.resolve(data);
    } );
    
  } */

  getAllTodos(){ // zebra
    
    // 'http://localhost:4000/todos'
    // let todosRequest = new Request( `${this.baseUrl}` ); 

    // let todosRequest = new Request( `/.netlify/functions/getAllTodos` ); 
    // let todosRequest = new Request( `/.netlify/functions/getalltodos/getalltodos` ); 
    let todosRequest = new Request( `/getalltodos` ); 

    let todosOptions = {};

    return this.customfetch( todosRequest, todosOptions )
    .then( (data) => { 
      // console.log( "getAllTodos... data" , data );
      return Promise.resolve( data );
    } );

  }
  
  massageThePhrase( str ){ // custom string helper function
    let offthefrontandback = str.trim();
    // let newstring = offthefrontandback.replace(/\s\s+/g, ' '); // alt 1
    let newstring = offthefrontandback.replace(/  +/g, ' '); // alt 2
    const firstChar = newstring.slice(0,1).toUpperCase();
    // const otherChars = newstring.slice(1).toLowerCase();
    const otherChars = newstring.slice(1);
    newstring = firstChar + otherChars;
    return newstring;
  }

}
export { TodoService };