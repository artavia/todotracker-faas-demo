import React , { useState, useEffect } from 'react';
import { NavLink } from 'react-router-dom';
import { TodoService } from '../services/TodoService';

import { withAuth } from '../functional-hoc/withAuth';

const EditTodo = ( props ) => {

  // console.log( "props" , props );

  const TServ = new TodoService();
  
  const [ todo_description, setTodo_description ] = useState('');
  const [ todo_responsible, setTodo_responsible ] = useState('');
  const [ todo_priority, setTodo_priority ] = useState('');
  const [ todo_completed , setTodo_completed ] = useState(''); 
  const [ error, setError ] = useState(false);

  // let id = props.match.params.id;

  useEffect( () => { 

    const fetchData = async () => {

      let id = props.match.params.id;

      return await TServ.getOneTodo(id)
      .then( async ( data ) => { 

        // console.log( "data" , data );

        // return data;
        const { todo_description , todo_responsible, todo_priority , todo_completed } = await data;
        setTodo_description( await todo_description );
        setTodo_responsible( await todo_responsible );
        setTodo_priority( await todo_priority );
        setTodo_completed( await todo_completed );
        // yeah, yea
      } )
      .catch( ( err ) => { // console.log( "err", err );
        setError( err.message );
      } );
      
    }; 

    fetchData();
  
  // }  ); // BIG TIME NO BECAUSE IT LEAKS
  } , []  ); // this fixes it

  const noOpLink = (event) => { event.preventDefault(); };

  const onChangeTodoDescription = ( event ) => { setTodo_description( event.target.value ); };
  
  const onChangeTodoResponsible = ( event ) => { setTodo_responsible( event.target.value ); };
  
  const onChangeTodoPriority = ( event ) => { setTodo_priority( event.target.value ); };

  const onChangeTodoCompleted = ( event ) => {
    setTodo_completed( !todo_completed );
  };

  const handleSubmit = async (event) => {
    
    event.preventDefault();

    const todoObject = { todo_description: TServ.massageThePhrase(todo_description), todo_responsible: TServ.massageThePhrase(todo_responsible), todo_priority: todo_priority, todo_completed: todo_completed }; 
    // console.log( "todoObject" , todoObject );

    let id = props.match.params.id;
    
    let editonetodopromise = await TServ.updateOneTodo( { id, todoObject } )
    .then( ( data ) => { // console.log( "data" , data );
      return data;
    } )
    .catch( ( err ) => { // console.log( "err", err );
      setError( err.message );
    } );

    // console.log( "await editonetodopromise" , await editonetodopromise );

    if( await editonetodopromise !== undefined ){
      props.history.push("/");
    }

  };

  let errormessage = (
    <>
      <div className="starter-template danger">
        <h2>An error has occurred</h2>
        <p>{ error }</p>
      </div>
    </>
  );

  let element = (
    <>
      <div className="jumbotron">
        <div className="container">
          <h1 className="display-3">Edit Todo Item</h1>   
          <p><NavLink className="btn btn-primary btn-lg" onClick={ noOpLink } to="#" role="button">Learn more &raquo;</NavLink></p>
        </div>
      </div>

      <div className="container">

      <form onSubmit={ handleSubmit }>
          
          <div className="form-group">
          <label htmlFor="description" className="form-check-label">Description</label>
            <input type="text" id="description" className="form-control" value={ todo_description } onChange={ onChangeTodoDescription } />
          </div>

          <div className="form-group">
            <label htmlFor="responsibility" className="form-check-label">Responsible</label>
            <input type="text" id="responsibility" className="form-control" value={ todo_responsible } onChange={ onChangeTodoResponsible } />
          </div>

          <div className="form-group">
            <div className="form-check form-check-inline">
              <input className="form-check-input" type="radio" name="priorityOptions" id="priorityLow" value="Low" checked={ todo_priority==="Low"} onChange={ onChangeTodoPriority } />
              <label htmlFor="priorityLow" className="form-check-label">Low</label>
            </div>

            <div className="form-check form-check-inline">
              <input className="form-check-input" type="radio" name="priorityOptions" id="priorityMedium" value="Medium" checked={ todo_priority==="Medium"} onChange={ onChangeTodoPriority } />
              <label htmlFor="priorityMedium" className="form-check-label">Medium</label>
            </div>

            <div className="form-check form-check-inline">
              <input className="form-check-input" type="radio" name="priorityOptions" id="priorityHigh" value="High" checked={ todo_priority==="High"} onChange={ onChangeTodoPriority } />
              <label htmlFor="priorityHigh" className="form-check-label">High</label>
            </div>
          </div>

          <div className="form-group">
            <div className="form-check form-check-inline">
              <input type="checkbox" className="form-check-input" id="completedCheckbox" name="completedCheckbox" onChange={ onChangeTodoCompleted } checked={ todo_completed } value={ todo_completed } />
              <label htmlFor="completedCheckbox" className="form-check-label">Completed</label>
            </div>
          </div>

          <div className="form-group">
            <input type="submit" value="Update Todo" className="btn btn-warning" disabled={ todo_description === '' || todo_responsible === '' || todo_priority === '' } />
          </div>

        </form>

      </div>

      { !!error && errormessage }

    </>
  );
  return element;
};


// export {EditTodo};

/**/ const AdminEditTodo = withAuth(EditTodo);
export {AdminEditTodo}; /**/