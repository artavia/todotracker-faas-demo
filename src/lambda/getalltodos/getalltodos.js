
// =============================================
// process.env SETUP
// =============================================
const { 
  CONNECTION_URL 
} = process.env; 

// BASE SETUP
// =============================================
// import mongoose from 'mongoose'; // original
const mongoose = require('mongoose');

// =============================================
// DB CONNECTION 
// =============================================
mongoose.connect( CONNECTION_URL , {
  useNewUrlParser: true 
  , useUnifiedTopology: true
  , useFindAndModify: false
  , useCreateIndex: true
} );
const db = mongoose.connection;
// db.on( 'open' , console.log.bind( console, `MongoDB database connection established` ) );
// db.once( 'open' , () => {
//   console.log(  `MongoDB database connection established` );
// } );
db.on( 'error' , console.error.bind( console, 'MongoDB connection error, bif!: ' ) );
// =============================================
// END CONNECTION 
// =============================================


// =============================================
// MODEL SCHEMA
// =============================================

// delete mongoose.connection.models['Todo'];

const schema = new mongoose.Schema( {
  todo_description: { type: String }
  , todo_responsible: { type: String }
  , todo_priority: { type: String }
  , todo_completed: { type: Boolean , default: false } 
} );

// const Todo = mongoose.model( 'Todo' , schema );
let Todo = mongoose.connection.models.Todo || mongoose.model('Todo', schema);

// =============================================
// END MODEL SCHEMA
// =============================================



exports.handler = async (event, context) => {

  // accessible through...
  // http://localhost:9000/.netlify/functions/getAllTodos
  
  // context.callbackWaitsForEmptyEventLoop = false;

  // your server-side functionality 
  // console.log( "context", context ); // { clientContext: {} }
  // console.log( "callback", callback );
  // console.log( "event.path", event.path ); // "Path parameter", e.g. -- /getAllTodos
  //// console.log( "event.httpMethod", event.httpMethod ); //"Incoming request's method name"
  // console.log( "event.headers", event.headers ); // {Incoming request headers}
  // console.log( "event.queryStringParameters", event.queryStringParameters ); // {query string parameters }
  //// console.log( "event.body", event.body ); // "A JSON string of the request payload."
  // console.log( "event.isBase64Encoded", event.isBase64Encoded ); // "A boolean flag to indicate if the applicable request payload is Base64-encode"
  
  try{
    
    const todos = await Todo.find(); // console.log( 'await todos: ', await todos );
    
    const netlifyresponseobject = {
      statusCode: 200
      , body: JSON.stringify( await todos ) 
    };
    
    return netlifyresponseobject;
    
  }
  catch(err){

    console.log( 'get todos err catch: ', err );
    
    const netlifyresponseerror = {
      statusCode: 500
      , body: JSON.stringify( { errormessage: err.message } )
    };
    
    return netlifyresponseerror;
  }
  //
};